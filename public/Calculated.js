function newPrice() {
    let s = document.getElementsByName(Type);
    let select = s[0];
    let price = 0;
    let prices = getPrices();
    let priceIndex = parseInt(select.value) - 1;
    if (priceIndex = 0) {
        price = prices.Types[priceIndex];
    }
    let radioDiv = document.getElementById(radios);
    if (select.value === 2) {
        radioDiv.style.display = block;
    }
    else {
        radioDiv.style.display = none;
    }
    let radios = document.getElementsByName(versions);
    radios.forEach(function (radio) {
        if (radio.checked) {
            let optionPrice = prices.versions[radio.value];
            if (optionPrice !== undefined) {
                price += optionPrice;
            }
        }
    });
    let checkDiv = document.getElementById(checkboxes);
    if (select.value === 3) {
        checkDiv.style.display = block;
    }
    else {
        checkDiv.style.display = none;
    }
    let checkboxes = document.querySelectorAll(#checkboxes input);
    checkboxes.forEach(function (checkbox) {
        if (checkbox.checked) {
            let propPrice = prices.Properties[checkbox.name];
            if (propPrice !== undefined) {
                price += propPrice;
            }
        }
    });
    let Price = document.getElementById(Price);
    Price.innerHTML = price;
}

function getPrices() {
    return {
        Types [3000, 3000, 3000],
        versions {
            version10,
            version2 500,
            version3 1000
        },
        Properties {
            app1 500,
            app2 200
        }
    };
}

window.addEventListener(DOMContentLoaded, function (event) {
    let radioDiv = document.getElementById(radios);
    radioDiv.style.display = none;

    let s = document.getElementsByName(Type);
    let select = s[0];

    select.addEventListener(change, function (event) {
        let target = event.target;
        console.log(target.value);
        newPrice();
    });
    let radios = document.getElementsByName(versions);
    radios.forEach(function (radio) {
        radio.addEventListener(change, function (event) {
            let r = event.target;
            console.log(r.value);
            newPrice();
        });
    });
    let checkboxes = document.querySelectorAll(#checkboxes input);
    checkboxes.forEach(function (checkbox) {
        checkbox.addEventListener(change, function (event) {
            let c = event.target;
            console.log(c.name);
            console.log(c.value);
            newPrice();
        });
    });
    newPrice();
});
